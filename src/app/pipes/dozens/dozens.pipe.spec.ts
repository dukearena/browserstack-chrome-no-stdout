import { DozensPipe } from './dozens.pipe';

describe('Pipe: Dozens', () => {
    let testSubject: DozensPipe;

    beforeEach(() => {
        testSubject = new DozensPipe();
    });

    it('create an instance', () => {
        expect(testSubject).toBeTruthy();
    });

    describe('transform', () => {
        it('should return "< 0" when the value is negative', () => {
            expect(testSubject.transform(-1)).toBe('< 0');
        });

        it('should return "0" when the value is 0', () => {
            expect(testSubject.transform(0)).toBe('0');
        });

        for (let i = 1; i < 10; i++) {
            it(`should return "${i}" when the value is ${i}`, () => {
                expect(testSubject.transform(i)).toBe(`${i}`);
            });
        }

        it('should return "1*10 + 0" when the value is 10', () => {
            expect(testSubject.transform(10)).toBe('1*10 + 0');
        });

        it('should return "2*10 + 5" when the value is 25', () => {
            expect(testSubject.transform(25)).toBe('2*10 + 5');
        });
    });
});
