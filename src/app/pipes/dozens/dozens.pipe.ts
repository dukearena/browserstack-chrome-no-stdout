import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'appDozens'
})
export class DozensPipe implements PipeTransform {
    transform(value: number): string {
        if (value < 0) {
            return '< 0';
        } else if (value < 10) {
            return `${value}`;
        } else {
            const tenUnits = Math.floor(value / 10);
            const units = value % 10;
            return `${tenUnits}*10 + ${units}`;
        }
    }
}
