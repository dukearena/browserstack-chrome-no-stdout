import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DozensPipe } from './dozens/dozens.pipe';

const EXPORTS = [DozensPipe];

@NgModule({
    declarations: [...EXPORTS, DozensPipe],
    exports: [...EXPORTS],
    imports: [CommonModule]
})
export class PipesModule {}
