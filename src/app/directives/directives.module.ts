import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CodeDirective } from './code/code.directive';

const EXPORTS = [CodeDirective];

@NgModule({
    declarations: [...EXPORTS],
    exports: [...EXPORTS],
    imports: [CommonModule]
})
export class DirectivesModule {}
