import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { CodeDirective } from './code.directive';

@Component({
    selector: 'app-host',
    template: `<div appCode>value</div>`
})
class HostComponent {}

describe('Directive: Code', () => {
    let testSubject: CodeDirective;
    let testSubjectElement: DebugElement;
    let hostFixture: ComponentFixture<HostComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [HostComponent, CodeDirective]
        }).compileComponents();
    }));

    beforeEach(() => {
        hostFixture = TestBed.createComponent(HostComponent);
        testSubjectElement = hostFixture.debugElement.query(By.css('div'));
        testSubject = testSubjectElement.injector.get(CodeDirective);
    });

    it('should create an instance', () => {
        expect(testSubject).toBeTruthy();
    });

    describe('HTML changes', () => {
        it('should apply "monospace" as font family', () => {
            hostFixture.detectChanges();
            expect(testSubjectElement.nativeElement.style.fontFamily).toBe('monospace');
        });

        it('should apply "rgb(242, 243, 246)" as background', () => {
            hostFixture.detectChanges();
            expect(testSubjectElement.nativeElement.style.backgroundColor).toBe('rgb(242, 243, 243)');
        });

        it('should apply "0px 16px" as padding', () => {
            hostFixture.detectChanges();
            expect(testSubjectElement.nativeElement.style.paddingLeft).toBe('16px');
            expect(testSubjectElement.nativeElement.style.paddingRight).toBe('16px');
            expect(testSubjectElement.nativeElement.style.paddingTop).toBe('0px');
            expect(testSubjectElement.nativeElement.style.paddingBottom).toBe('0px');
        });
    });
});
