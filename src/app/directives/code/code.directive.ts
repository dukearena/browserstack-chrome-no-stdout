import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
    selector: '[appCode]'
})
export class CodeDirective {
    @HostBinding('style.font-family')
    fontFamily: string = 'monospace';

    @HostBinding('style.background-color')
    backgroundColor: string = 'rgb(242, 243, 243)';

    @HostBinding('style.padding')
    padding: string = '0 16px';
}
