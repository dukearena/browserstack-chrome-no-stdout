import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PipesModule } from '../pipes/pipes.module';
import { CounterComponent } from './counter/counter.component';

const EXPORTS = [CounterComponent];

@NgModule({
    declarations: [...EXPORTS],
    exports: [...EXPORTS],
    imports: [CommonModule, BrowserAnimationsModule, PipesModule, MatButtonModule]
})
export class ComponentsModule {}
