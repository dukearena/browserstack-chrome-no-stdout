import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-counter',
    templateUrl: './counter.component.html',
    styleUrls: ['./counter.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CounterComponent implements OnInit {
    value: number = 0;

    constructor() {}

    ngOnInit() {}

    changeValue(valueToAdd: number) {
        this.value += valueToAdd;
        if (this.value < 0) {
            this.value = 0;
        }
    }
}
