import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DozensFakePipe } from 'src/test-fakes';
import { CounterComponent } from './counter.component';

describe('CounterComponent', () => {
    let component: CounterComponent;
    let fixture: ComponentFixture<CounterComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CounterComponent, DozensFakePipe],
            imports: [NoopAnimationsModule, MatButtonModule]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CounterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('changeValue()', () => {
        const clickOnAddButton = () => {
            const buttonElement = fixture.debugElement.query(By.css('.app-counter__add'));
            buttonElement.triggerEventHandler('click', null);
        };

        const clickOnRemoveButton = () => {
            const buttonElement = fixture.debugElement.query(By.css('.app-counter__remove'));
            buttonElement.triggerEventHandler('click', null);
        };

        const clickOnResetButton = () => {
            const buttonElement = fixture.debugElement.query(By.css('.app-counter__reset'));
            buttonElement.triggerEventHandler('click', null);
        };

        it('should be called from app-counter__add with +1', () => {
            const changeValueSpy = spyOn(component, 'changeValue');
            clickOnAddButton();
            expect(changeValueSpy).toHaveBeenCalledWith(1);
        });

        it('should be called from app-counter__remove with -1', () => {
            const changeValueSpy = spyOn(component, 'changeValue');
            clickOnRemoveButton();
            expect(changeValueSpy).toHaveBeenCalledWith(-1);
        });

        it('should be called from app-counter__reset with negative value', () => {
            const changeValueSpy = spyOn(component, 'changeValue');
            component.value = 100;
            clickOnResetButton();
            expect(changeValueSpy).toHaveBeenCalledWith(-100);
        });

        it('should update app-counter__value with the parameter', () => {
            component.value = 5;
            clickOnAddButton();
            clickOnAddButton();
            clickOnAddButton();

            fixture.detectChanges();
            const valueElement = fixture.debugElement.query(By.css('.app-counter__value'));
            expect(valueElement.nativeElement.innerText).toBe('8');
        });
    });
});
