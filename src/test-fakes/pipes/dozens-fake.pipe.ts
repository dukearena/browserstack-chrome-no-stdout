import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'appDozens' })
export class DozensFakePipe implements PipeTransform {
    transform: jasmine.Spy = jasmine.createSpy('transform').and.callFake(value => value);
}
