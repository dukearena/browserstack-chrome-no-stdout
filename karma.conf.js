// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html
const path = require('path');

module.exports = function (config) {
    const IS_DEV = process.env.NODE_ENV ? process.env.NODE_ENV.trim() === 'dev' : false;

    const ipaddress = function () {
        const os = require('os');
        if (IS_DEV) {
            return '127.0.0.1';
        }

        var interfaces = os.networkInterfaces();
        var addresses = [];
        for (var k in interfaces) {
            for (var k2 in interfaces[k]) {
                var address = interfaces[k][k2];
                if (address.family === 'IPv4' && !address.internal) {
                    addresses.push(address.address);
                }
            }
        }
        if (addresses.length > 0) {
            return addresses[0];
        } else {
            return '127.0.0.1';
        }
    };

    config.set({
        browserStack: {
            username: '******',
            accessKey: '******',
            name: 'another-run',
            build: 'bs-chrome-no-stdout-test'
        },
        basePath: '',
        frameworks: ['jasmine', '@angular-devkit/build-angular'],
        plugins: [
            require('karma-jasmine'),
            require('karma-browserstack-launcher'),
            require('karma-chrome-launcher'),
            require('karma-jasmine-html-reporter'),
            require('karma-junit-reporter'),
            require('karma-coverage-istanbul-reporter'),
            require('@angular-devkit/build-angular/plugins/karma')
        ],
        client: {
            clearContext: false, // leave Jasmine Spec Runner output visible in browser
            jasmine: {
                random: false
            }
        },
        coverageIstanbulReporter: {
            dir: require('path').join(__dirname, 'coverage'),
            reports: ['html', 'lcovonly'],
            dir: path.join(__dirname, 'coverage'),
            fixWebpackSourcePaths: true
        },
        files: [{ pattern: 'src/assets/*.svg', watched: false, served: true, included: false }],
        proxies: {
            '/cross_16x16.svg': '/base/src/assets/cross_16x16.svg',
            '/not_existing.svg': '/base/src/assets/not_existing.svg'
        },
        angularCli: {
            environment: 'dev'
        },
        reporters: ['progress', 'kjhtml', 'junit'],
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        singleRun: true,
        hostname: ipaddress(),
        port: 9876,
        browsers: ['Chrome'],
        // a list of BrowserStack Selenium Grid when running against multiple browsers
        customLaunchers: {
            bs_chrome_windows: {
                base: 'BrowserStack',
                browser: 'chrome',
                os: 'Windows',
                os_version: '10'
            }
        },
        captureTimeout: 900000,
        browserSocketTimeout: 900000,
        browserNoActivityTimeout: 900000,
        browserDisconnectTolerance: 3,
        browserDisconnectTimeout: 900000,

        junitReporter: {
            outputDir: 'test-reports/any-ui', // results will be saved as $outputDir/$browserName.xml
            outputFile: 'report.xml', // if included, results will be saved as $outputDir/$browserName/$outputFile
            suite: '', // suite will become the package name attribute in xml testsuite element
            useBrowserName: false, // add browser name to report and classes names
            nameFormatter: undefined, // function (browser, result) to customize the name attribute in xml testcase element
            classNameFormatter: undefined, // function (browser, result) to customize the classname attribute in xml testcase element
            properties: {} // key value pair of properties to add to the <properties> section of the report
        }
    });
};
