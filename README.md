# BrowserstackChromeNoStdout

## How to setup the project

- git clone https://gitlab.com/dukearena/browserstack-chrome-no-stdout.git
- `npm install`
- `npm run serve` to explore the UI and be sure it's working
- `npm run test:local` to run the tests in local and be sure they works
- setup the `karma.json` file configuring as the following example:

```javascript
config.set({
    ...
    browserStack: {
        username: '[bs_username]',
        accessKey: '[bs_access_key]',
        name: 'another-run',
        build: 'bs-chrome-no-stdout-test'
    },
    ...
});
```

## How to run the issue
`npm run test:browserstack` to run the tests in browserstack using the following parameters:

- **Build id:**: 6e6ea1eac0f9546e10f8bba235bf55a0b2a41b39
- **Build Name**: bs-chrome-no-stdout-test
- **Run name**: another-run
